<?php
require_once './vendor/autoload.php';

use CodeMax\distro\Distro;

$distro = new Distro( __DIR__.'/distro-config.json' );

$distro->generate();
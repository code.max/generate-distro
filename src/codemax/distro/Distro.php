<?php
namespace codemax\distro;

use codemax\tool\Path;
use codemax\tool\TXT;
use codemax\tool\json\entities\JsonEntity;
use ErrorException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Distro extends JsonEntity
{
    
    /**
     * 
     * @param string $filepath
     * @param bool $lazyLoad
     */
    public function __construct( string $filepath, bool $lazyLoad = false )
    {
        parent::__construct( $filepath, $lazyLoad );
        
        if( !isset( $this->data()->id ) )
        {
            $this->id( dirname( $this->source()->path() ) );
        }
    }
    
    /**
     * 
     * @param string $id
     * @return string
     */
    public function id( string $id = NULL ): string
    {
        if ( isset( $id ) )
        {
            $this->data()->id = $id;
        }
        return $this->data()->id;
    }
    
    /**
     * 
     * @return DistroDestination
     */
    public function destination(): DistroDestination
    {
        return new DistroDestination( $this->data()->destination );
    }
    
    /**
     * 
     * @return DistroSource
     */
    public function source(): DistroSource
    {
        return new DistroSource( $this->data()->source );
    }
    
    /**
     * 
     */
    public function generate()
    {
        $sourcePath = Path::safe( $this->source()->path() );
        $sourceIgnoreFiles = $this->source()->ignoreFiles();
        $sourceIgnoreFolders = $this->source()->ignoreFolders();
        
        /* Distributions container folder is required */
        if ( !file_exists( $this->destination()->path() ) )
        {
            throw new ErrorException( "{$this->destination()->path()} - not found!" );
        }
        
        $destinationPath = Path::safe( "{$this->destination()->path()}/{$this->id()}/{$this->composerVersion()}" );
        
        /* if the path 'container/project/version' exists, the version folder will be deleted */
        if ( file_exists( $destinationPath ) )
        {
            Path::deleteEntireDirectory( $destinationPath );
        }
        
        $projectFolder = Path::safe( "{$this->destination()->path()}/{$this->id()}" );
        if ( !file_exists( $projectFolder ) )
        {
            mkdir( $projectFolder );
        }
        
        $versionFolder = $destinationPath;
        if ( !file_exists( $versionFolder ) )
        {
            mkdir( $versionFolder );
        }
        
        $ignoredFiles = [];
        $ignoredFolders = [];
        $filesOfVersion = [];
        $rii = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $sourcePath ) );
        
        foreach ( $rii as $file )
        {
            $txtCurrentFile = TXT::create( $file->getPathname() );
            
            if ( !$txtCurrentFile->endWith( '.' ) && !$txtCurrentFile->endWith( '..' ) )
            {
                $relativePath = $txtCurrentFile->lastPart( $sourcePath );
                $matched = FALSE;
                if( in_array( $relativePath, $sourceIgnoreFiles ) )
                {
                    $ignoredFiles[] = $relativePath;
                    $sourceIgnoreFiles = array_diff( $sourceIgnoreFiles, [ $relativePath ] );
                    $matched = TRUE;
                }
                if( !$matched )
                {
                    $txtRelativePath =  TXT::create( $relativePath );
                    foreach ( $sourceIgnoreFolders as $prefix )
                    {
                        if ( $txtRelativePath->startWith( $prefix ) )
                        {
                            $ignoredFolders[] = $relativePath;
                            $matched = TRUE;
                            break;
                        }
                    }
                }
                if ( !$matched )
                {
                    $filesOfVersion[] = $relativePath;
                    Path::forceCreateFilepath( "{$destinationPath}{$relativePath}" );
                    copy( 
                        Path::safe( "{$sourcePath}{$relativePath}" ), 
                        Path::safe( "{$destinationPath}{$relativePath}" ) 
                    );
                }
            }
        }
        
        print "Summary: ";
        print sizeof( $filesOfVersion ).' files copied - ';
        print ( sizeof( $ignoredFiles ) + sizeof( $ignoredFolders ) ).' files ignored. ';
        print PHP_EOL;
        
    }
    
    /**
     * 
     * @return string
     */
    public function composerVersion(): string
    {
        $composer = new JsonEntity( "{$this->source()->path()}/composer.json" );
        return $composer->data()->version ?? 'x.x.x';
    }
    
}
<?php
namespace codemax\distro;

use codemax\tool\json\entities\models\JsonModel;
use stdClass;

class DistroSource extends JsonModel
{
    /**
     * 
     * @param array|stdClass|null $jsonSource
     */
    public function __construct( $jsonSource = NULL )
    {
        parent::__construct( $jsonSource );
        
        if ( !isset( $this->data()->path ) )
        {
            $this->data()->path = "";
        }
        if ( !isset( $this->data()->ignore ) )
        {
            $this->data()->ignore = [];
        }
        if ( !isset( $this->data()->ignore->files ) )
        {
            $this->data()->ignore->files = [];
        }
        if ( !isset( $this->data()->ignore->folders ) )
        {
            $this->data()->ignore->folders = [];
        }
    }
    
    /**
     * 
     * @param string $path
     * @return string
     */
    public function path( string $path = null ) : string
    {
        if ( isset( $path ) )
        {
            $this->data()->path = $path;
        }
        return $this->data()->path;
    }

    /**
     * 
     * @param array $listOfFilesPath
     * @return array
     */
    public function ignoreFiles( array $listOfFilesPath = null ) : array
    {
        if ( isset( $listOfFilesPath ) )
        {
            $this->data()->ignore->files = $listOfFilesPath;
        }
        return $this->data()->ignore->files;
    }  

    /**
     * 
     * @param array $listOfFoldersPath
     * @return array
     */
    public function ignoreFolders( array $listOfFoldersPath = null ) : array
    {
        if ( isset( $listOfFoldersPath ) )
        {
            $this->data()->ignore->folders = $listOfFoldersPath;
        }
        return $this->data()->ignore->folders;
    }
}